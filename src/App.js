import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import Home from './components/views/Home';
import ProjectShow from './components/views/ProjectShow';
import Error from './components/views/Error';

import './App.css';

function App() {

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path = "/projects/:id" component={ProjectShow}>
          <ProjectShow />
        </Route>
        <Route path = "/error" component={Error}>
          <Error />
        </Route>

      </Switch>
    </Router>
  );
}

export default App;
