import React, {useState, useEffect} from 'react';
import Tile from './Tile';

function Section({category}) {
    return (
        <div className="row section mb-5">
            <div className="col-12">
                <h3 className="section__title">{category.Category_title}</h3>
                <p className="section__intro">{category.Category_intro}</p>
            </div>
            
            {/* Product Tiles */}
            {category.projectList.map((project, index) => {
                return(
                    <Tile project={project} key={index} />
                )
            })}
            <div className="col-12 text-right">
                <a className="top-button" href="#">BACK TO TOP</a>
            </div>
        </div>
    )
}

export default Section;