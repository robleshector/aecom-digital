import React from 'react';
import {useHistory} from 'react-router-dom';

function Tile({project}) {
    const history = useHistory();

    const handleProjectClick = e => {
        return (
           history.push("/projects/" + e.target.parentElement.dataset.projectid)
        )
    }


    return (
        <div className="col-sm-6 col-md-4 col-lg-3 p-2 project" >
            <div className="project__tile" data-projectid={project.id} onClick={handleProjectClick}>
                <img src={"https://apps.aecom-digital.com/excellence"+project.image.url} alt={project.image.name} className="project__img"/>
                <h5 className="project__title">{project.title}</h5>
            </div>
        </div>
    )
}

export default Tile;