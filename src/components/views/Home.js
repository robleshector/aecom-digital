import React, {useState, useEffect} from 'react';
import Section from './../layouts/Section';

function Home() {

    const [categories, setCategories] = useState([]);
    const [selectedProject, setSelectedProject] = useState(null);

    useEffect(() => {
        fetch('https://apps.aecom-digital.com/excellence/projects', {
        method: "GET",
        header: {
            "Content-Type" : "application/json"
        }
        })
        .then(data => data.json())
        .then(projects => {
        // I have no access to Categories so I had to get them from the products
        // Check for all Categories
        let foundCategories = [];
        projects.forEach(project => {
            project.categories.forEach(projectCategory => {
            let isUnique = true;
            foundCategories.forEach(category => {
                if(projectCategory.id === category.id) {
                isUnique = false;
                }
            });
    
            if(isUnique) {
                foundCategories.push(projectCategory);
            }
            })
        })

        foundCategories.forEach(category => {
            category.projectList = [];
            projects.forEach(project => {
            project.categories.forEach(projectCategory => {
                if(projectCategory.id === category.id) {
                category.projectList.push(project);
                }
            })
            })
        })
        foundCategories.sort(function(a, b){
            if(a.Category_title < b.Category_title) { return -1; }
            return 0;
        })

        setCategories(foundCategories)
        })
    },[])


    return (
        <div className="container py-4 px-3">
            {categories.map((category, index) => {
                return (
                    <Section category={category} key={index}/>
                )
            })}
        </div>
    )
}

export default Home;