import React from 'react';


function Error() {

    return (
        <div className="container">
            <h2 className="error">Oops! Something went wrong.</h2>
        </div>
    )
}

export default Error;