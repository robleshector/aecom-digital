import React, {useState, useEffect} from 'react';
import { useParams} from 'react-router';
import {Link} from 'react-router-dom';
import {useHistory} from 'react-router-dom';

function ProjectShow() {
    const history = useHistory();
    const [project, setProject] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    let {id} = useParams();

    useEffect(() => {
        setIsLoading(true);
        fetch('https://apps.aecom-digital.com/excellence/projects/' + id, {
            method: "GET",
            header: {
                "Content-Type" : "application/json"
            }
        })
        .then(data => data.json())
        .then(project => {
            setProject(project);
            setIsLoading(false);
        })
        .catch(err => {
            window.location.replace("/error");
        })
    },[])

    if(project && !isLoading) {
        return (
            <div className="container py-4 px-3 projectShow">
                <div className="text-right close-btn">
                    <Link to="/">&times;</Link>
                </div>
                <div className="row">
                    <div className="col-lg-8 mx-lg-auto">
                        <h3 className="projectShow__title mb-5">{project.title}</h3>
                        <p className="projectShow__text mb-4">{project.project_text}</p>
                        <img src={"https://apps.aecom-digital.com/excellence"+project.image.url} alt={project.image.name} className="projectShow__image img-fluid"/>
                    </div>

                </div>
            </div>
        )
    } else {
        return(
            <div className="container py-4 px-3">
                <h3>Loading Project</h3>
            </div>
        )
    }
}

export default ProjectShow;